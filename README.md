## Install dependencies
1.  npm install

## Getting Started

1. Start App:
    ```
    npm start
    ```

2. Test:
    ```
    npm test
    ```
## Using Docker

1. docker-compose build
2. docker-compose up
