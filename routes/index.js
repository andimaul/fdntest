var express = require('express');
var router = express.Router();
var {findData,pivotData,checkConnection} = require('../repository')

router.get('/test', async (req, res) => {
  const result = "ok"
  checkConnection()
  res.send(result)
});

router.get('/get-data', async (req, res) => {
  const result = await findData()
  res.send(result)
});

router.get('/pivot', async (req,res) => {
  const result = await pivotData()
  res.render('pivot')
})

router.get('/pivot-data', async (req,res) => {
  const result = await pivotData()
  res.send(result)
})

// router.post('/post',async (req,res) => {
//   console.log(req.body)
//   await postData(req.body)
//   res.send("SUKSES")            
// })

module.exports = router;


