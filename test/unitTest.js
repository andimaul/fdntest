process.env.NODE_ENV = 'test';

const {checkConnection,postData,close} = require('../repository');

let chai = require('chai').expect;
let chaiHttp = require('chai-http');
let server = require('../app');
const request = require('supertest');

describe('GET /data', () => {
    before((done) => {
        checkConnection()
        .then(() => done())
        .catch((err) => done(err));
    })
  
    after((done) => {
      close()
        .then(() => done())
        .catch((err) => done(err));
    })
  
    it('OK, getting pivot data', (done) => {
      request(server).get('/pivot-data')
        .then((res) => {
          const body = res.body;
          chai(body).not.to.be.empty;
          done();
        })
        .catch(done);
    });
  
    it('OK, getting table data', (done) => {
      request(server).get('/get-data')
        .then((res) => {
          const body = res.body;
          chai(body).not.to.be.empty;
          done();
        })
        .catch(done);
    });
  })

