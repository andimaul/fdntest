const {Sequelize,Model,QueryTypes,DataTypes} = require('sequelize');
const sequelize = new Sequelize(process.env.DB_SCHEMA || 'warehouse',
                                process.env.DB_USER || 'postgres',
                                process.env.DB_PASSWORD || 'maulana7',
                                {
                                    host: 'localhost',
                                    port: 5432,
                                    dialect: 'postgres',
                                    dialectOptions: {
                                        ssl: process.env.DB_SSL == "true"
                                    }
                                });
// const sequelize = new Sequelize('postgres://postgres:maulana7@postgres:5432/warehouse')
class Fdn extends Model {}

Fdn.init({
  // Model attributes are defined here
  firstname: {
    type: DataTypes.STRING,
  },
  lastname: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  item: {
    type: DataTypes.STRING
  },
  quantity: {
    type: DataTypes.INTEGER
  },
  total_price: {
    type: DataTypes.DECIMAL
  }
}, {
  // Other model options go here
  sequelize, // We need to pass the connection instance
  modelName: 'fdn', // We need to choose the model name
  freezeTableName: true,
  timestamps: false
});

module.exports.checkConnection = async () => {
    try {
        await sequelize;
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:',error);
      }
}  


module.exports.close = async () => {
  try {
  await sequelize.close()
  console.log("close connection")
} catch (error) {
  console.error('Unable to close the database:',error);
}
}

module.exports.postData = async (req) => {
    await sequelize.sync();

    const result = await Fdn.create(req).then((result)=>{

            console.log("SUKSES")

        }).catch((err)=>{

            console.log(err)

        });
    }


module.exports.findData = async () => {
    const result = await sequelize.query("SELECT CONCAT(FIRSTNAME || ' ' || LASTNAME) AS FULLNAME,email,item,quantity,total_price FROM fdn;");
    // console.log(data)
    return result;
}

module.exports.pivotData = async () => {
    const result = await sequelize.query("select CONCAT(FIRSTNAME || ' ' || LASTNAME) AS FULLNAME,email,coalesce(sum(case when item = ? then quantity end), 0) AS barang1,coalesce(sum(case when item = ? then quantity end), 0) AS barang2,coalesce(sum(case when item = ? then quantity end), 0) AS barang3,coalesce(sum(case when item = ? then quantity end), 0) AS barang4,coalesce(sum(case when item = ? then quantity end), 0) AS barang5,coalesce(sum(case when item = ? then quantity end), 0) AS barang6,coalesce(sum(case when item = ? then quantity end), 0) AS barang7,coalesce(sum(case when item = ? then quantity end), 0) AS barang8,coalesce(sum(case when item = ? then quantity end), 0) AS barang9,coalesce(sum(case when item = ? then quantity end), 0) AS barang10 from fdn group by firstname,lastname,email order by 3 desc,4 desc,5 desc,6 desc,7 desc,8 desc,9 desc,10 desc;",{
      replacements : ['barang1','barang2','barang3','barang4','barang5','barang6','barang7','barang8','barang9','barang10'],
      type: QueryTypes.SELECT
    });
    return result
}

// module.exports = {"checkConnection":checkConnection,"close":close};
